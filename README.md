<!-- Clear both -->
<br clear="both">

<!-- Title -->
<h1 align="left">Hello! I'm Gustavo 👋</h1>

<!-- Welcome -->
<p align="left">Welcome to my space on GitLab! I'm a student of Multiplatform Application Development at Ilerna Barcelona, currently residing in Barcelona.</p>

<!-- About me -->
<h2 align="left">About me</h2>

<!-- Personal details -->
<p align="left">🌍 Location: Barcelona, Spain<br>💼 Occupation: Student of Multiplatform Application Development at Ilerna Barcelona<br>🎓 Education:I am currently studying Multiplatform Application Development.</p>

<!-- Skills -->
<h2 align="left">Skills</h2>

<!-- Skill icons -->
<div align="left">
  <p>I'm learning:</p>
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/c/c-original.svg" height="40" alt="c logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original.svg" height="40" alt="java logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" height="40" alt="python logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" height="40" alt="html5 logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" height="40" alt="css3 logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" height="40" alt="javascript logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" height="40" alt="bootstrap logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original.svg" height="40" alt="mysql logo" />
</div>

<!-- IDE -->
<h2 align="left">IDE</h2>

<!-- IDE icons -->
<div align="left">
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vim/vim-original.svg" height="40" alt="vim logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" height="40" alt="vscode logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/intellij/intellij-original.svg" height="40" alt="intellij logo" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/pycharm/pycharm-original.svg" height="40" alt="pycharm logo" />
</div>

<!-- Statistics -->
<h2 align="left">Statistics</h2>

<!-- Statistics graphs -->
<div align="center">
  <img src="https://github-readme-stats.vercel.app/api?username=gusgonza&hide_title=false&hide_rank=false&show_icons=true&include_all_commits=true&count_private=true&disable_animations=false&theme=dracula&locale=en&hide_border=false&order=1" height="150" alt="stats graph" />
  <img src="https://github-readme-stats.vercel.app/api/top-langs?username=gusgonza&locale=en&hide_title=false&layout=compact&card_width=320&langs_count=5&theme=dracula&hide_border=false&order=2" height="150" alt="languages graph" />
  <img src="https://streak-stats.demolab.com?user=gusgonza&locale=en&mode=daily&theme=dracula&hide_border=false&border_radius=5&order=3" height="150" alt="streak graph" />
  <img src="https://github-profile-trophy.vercel.app?username=gusgonza&theme=dracula&column=-1&row=1&margin-w=8&margin-h=8&no-bg=false&no-frame=false&order=4" height="150" alt="trophy graph" />
</div>
